package com.higgs.tmq.servletsecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public class SimpleServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(SimpleServlet.class);

    private long threadId = -1L;

    public SimpleServlet() {
        logger.debug("Constructor...");
    }
//public SimpleServlet(String a) {
//    logger.debug("Constructor..." + a);
//}

    @Override
    public void init() throws ServletException {
        super.init();
        logger.debug("init...");
        threadId = 0L;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        System.out.println("init(param)");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("service...");
        super.service(req, resp);
        PrintWriter writer = resp.getWriter();
        writer.print("Done");

        logger.debug("thread id = " + threadId);
        threadId = Thread.currentThread().getId();
        logger.debug("Thread # " + threadId + " is doing this task");
    }

    // ---------------- Action -----------------------------------------------------------------------------------------
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        logger.debug("doPost...");
        res.setContentType("application/json");
//        try {
//            TimeUnit.SECONDS.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        res.getWriter().print("Hung");
    }

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        logger.debug("doGet...");
//    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doDelete...");
        super.doDelete(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doPut...");
        super.doPut(req, resp);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public void destroy() {
        logger.debug("destroy...");
        super.destroy();
    }
}
