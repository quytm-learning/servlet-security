package com.higgs.tmq.servletsecurity.util;

public class Facebook {
    private final static String CLIEND_ID = "323112341518471";
    private final static String SECRET_ID = "35b8a4a375e42f2732b3119fd45869ff";
    private final static String REDIRECT_LOGIN_SUCCESS = "http://localhost:8081/authen";

    private final static String SCOPE = "public_profile user_friends";

    public static String getInfo(String token) {
        return "https://graph.facebook.com/me?fields=email&access_token=" + token;
    }

    public static String getToken(String code) {
        return String.format("https://graph.facebook.com/oauth/access_token?client_id=%1$s&client_secret=%2$s&code=%3$s&redirect_uri=%4$s",
                CLIEND_ID,
                SECRET_ID,
                code,
                REDIRECT_LOGIN_SUCCESS);
    }

    public static String sendRequest() {
        String state = "1";
        return String.format("https://www.facebook.com/dialog/oauth?client_id=%1$s&redirect_uri=%2$s&scope=%3$s&state=%4$s",
                CLIEND_ID,
                REDIRECT_LOGIN_SUCCESS,
                SCOPE,
                state);
    }
}