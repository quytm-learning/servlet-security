package com.higgs.tmq.servletsecurity.servlet;

import com.google.gson.Gson;
import com.higgs.tmq.servletsecurity.model.AccessToken;
import com.higgs.tmq.servletsecurity.util.Facebook;
import com.higgs.tmq.servletsecurity.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class AuthenServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(AuthenServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String code = req.getParameter("code");
        if (StringUtils.isEmpty(code)) {
            logger.info(req.getMethod());
        } else {
            String content = Utils.getUrlContents(Facebook.getToken(code));
            logger.info(content);

            Gson g = new Gson();
            AccessToken p = g.fromJson(content, AccessToken.class);
            logger.info(p.getAccess_token());
            resp.getWriter().println(p.getAccess_token());
            req.getSession().setAttribute("access_token", p.getAccess_token());
        }
    }
}
