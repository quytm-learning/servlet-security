package com.higgs.tmq.servletsecurity.servlet;

import com.higgs.tmq.servletsecurity.util.Facebook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFBServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(LoginFBServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String url = Facebook.login();
        String url = Facebook.sendRequest();
        resp.sendRedirect(url);
    }
}
