package com.higgs.tmq.servletsecurity.servlet;

import com.higgs.tmq.servletsecurity.util.Facebook;
import com.higgs.tmq.servletsecurity.util.Utils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = (String) req.getSession().getAttribute("access_token");
        if (StringUtils.isNotEmpty(token)) {
            String url = Facebook.getInfo(token);
            String response = Utils.getUrlContents(url);
            resp.getWriter().println(response);
        }
    }

}
