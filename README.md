# Servlet security
Authentication, Authorization, OAuth2

## Authentication

Authentication  is the act of confirming the truth of an attribute of a single piece of data claimed true by an entity.
Là quá trình xác định và xác nhận danh tính của một khách truy cập vào một ứng dụng.

Có nhiều cách để xác định user: thông qua username-password, public-key hay sinh trắc học (vân tay, mắt, mặt, ...)

## Authorization

Authorization is the function of specifying access rights/privileges to resources related to information security and computer security in general and to access control in particular
Authorization: Là quá trình xác định xem một người dùng có quyền truy cập một URL / tài nguyên cụ thể hoặc để thực hiện một số hành động hay không

Thường thì người dùng sẽ được phân ra các role khác nhau: Admin, Franchisee, Customer, ... và tùy theo đó mà hệ thống cho phép từng loại user có thể thực hiện một số quyền khác nhau.

## Servlet

Servlet security
- Đối tượng nào sẽ thực hiện việc implement security? -> Tomcat or Application (Servlet chỉ việc xử lý request chứ không can thiệp vào trong cơ chế bảo mật)

- Config security: ta có thể config security ở trong tomcat hoặc trong file web.xml
- Tomcat-user.xml (memory realm): 
	+ Có tác dụng lên mọi app chạy trên nó, mặc dù nó tốt cho việc testing, nhưng nó không được khuyên dùng cho bản production bởi vì mỗi lần có update thì bắt buôc phải restart server mới có thể apply thay đổi đó
	+ Các config được khai báo ở trong cặp thẻ <tomcat-user></tomcat-user>
	+ Trong đó, ta cần khai báo 2 properties: role và user (có username, password, role)
- DD file:
	+ Để enable authentication, DD cần có thẻ <login-config>, trong đó có <auth-method>BASIC</auth-method>
	
## OAuth2 

### Khái niệm

OAuth là viết tắt của Open Authorization, theo trang web chính thức thì OAuth là:

An open protocol to allow secure authorization in a simple and standard method from web, mobile and desktop applications.

(tạm dịch):

Một giao thức mở nhằm cung cấp cách thức đơn giản và tiêu chuẩn cho các thao tác xác thực an toàn trong các ứng dụng trên web, mobile & desktop.

- **Resource**: là các thông tin người dùng trên những dịch vụ cung cấp xác thực qua OAuth (Facebook, Google+, Twitter,…)
- **Resource owner**: chính là người dùng sở hữu những resource trên.
- **Resource server**: là server của các dịch vụ cung cấp xác thực qua OAuth 
- **Client application**: là ứng dụng của chúng ta muốn truy cập các resource trên resource server.
- **Authorization server**: là server dùng để nhận lại các thông tin đã xác thực của người dùng, trong đó quan trọng nhất là access token
- **Access token**: là 1 chuỗi ký tự được mã hóa, có giá trị trong 1 thời hạn nhất định, dùng để thay thế cho toàn bộ quá trình xác thực của người dùng
- **Scope**: người dùng thường cung cấp nhiều thông tin (email, tên, tuổi, địa chỉ, giới tính,…).
- **Strategy**: là 1 tập hợp bao gồm tất cả các định nghĩa trên cho từng dịch vụ, ví dụ Facebook strategy, Google+ strategy, Twitter strategy,…

### Flow

```
     +--------+                               +---------------+
     |        |--(A)- Authorization Request ->|   Resource    |
     |        |                               |     Owner     |
     |        |<-(B)-- Authorization Grant ---|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(C)-- Authorization Grant -->| Authorization |
     | Client |                               |     Server    |
     |        |<-(D)----- Access Token -------|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(E)----- Access Token ------>|    Resource   |
     |        |                               |     Server    |
     |        |<-(F)--- Protected Resource ---|               |
     +--------+                               +---------------+
     
```

